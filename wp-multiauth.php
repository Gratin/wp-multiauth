<?php 
/*
Plugin Name: Wordpress Multiauth
Description: Add multi-factor authentication to wordpress
Plugin URI: 
Author: Luc-Olivier Noel
Author URI: https://lucolivienoel.com
Version: 1.0
*/


// require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

if (!session_id()) {
	session_start();
}

class MultiAuth
{
	// Version pour permettre la mise a jour du plugin si necessaire
	private $version = '0.3';
	private $pluginName = "";

	public function __construct()
	{
		// Fonction appeler au hook init de wordpress, on mets la logique plus generique dans cette fonction
		add_action('init', [$this, 'init']);
		add_filter('query_vars', [$this, 'addQueryVars']);
		register_activation_hook( __FILE__, [$this, 'customRoutes']);
	}

	/**
	 * Called when plugin is initiatied
	 * @return void
	 */ 
	public function init()
	{
		// Si la version de la classe est recente que la BD, on update
		if (!$this->checkVersion()) {
			$this->install();
		}

		// Ajout des routes pour les cours
		// $this->customRoutes();

		add_filter('login_redirect', [$this, 'redirectAuth'], 10, 3);
		add_filter('template_redirect', [$this, 'forceAuth'], 10, 1);

		add_action('show_user_profile', [$this, 'showUserProfile']);
		add_action('edit_user_profile', [$this, 'editUserProfile']);
	}

	/**
	 * Validates class/plugin version with db version
	 * @return mixed(int|bool)
	 */ 
	public function checkVersion()
	{
		$version = get_option($this->pluginName.'_version');
		if (!$version) {
			return false;
		}

		return $version;
	}

	/**
	 * Installs plugin
	 * @return void
	 */ 
	private function install()
	{
		// Logique d'instantiation des tables, etc. 
		update_option($this->pluginName.'_version', $this->version);
	}

	public function addQueryVars($vars)
	{
		$vars[] = 'auth';
		return $vars;
	}

	public function enableAuth()
	{
		$user = wp_get_current_user();

		include __DIR__.'/src/OTP.php';
		$otp = new OTP();

		if (get_user_meta($user->ID, 'auth_enabled', true)) {
			$secret = get_user_meta($user->ID, 'auth_secret', true);
		} else {
			$secret = $otp->createSecret();
			update_user_meta($user->ID, 'auth_enabled', true);
			update_user_meta($user->ID, 'auth_secret', $secret);
		}

		$qrCodeData = $otp->getQRCodeGoogleUrl(get_bloginfo('name'), $secret, 'MFA');

		require_once __DIR__.'/vendor/autoload.php';
		$code = new \Endroid\QrCode\QrCode($qrCodeData);
		echo $code->writeDataUri();
		die;
	}

	public function disableAuth()
	{
		$userId = get_current_user_id();
		update_user_meta($userId, 'auth_secret', '');
		update_user_meta($userId, 'auth_enabled', false);
	}

	public function redirectAuth($redirect, $request, $user)
	{
		if (isset($user->errors) || !$user) {
			return $redirect;
		}

		$authEnabled = get_user_meta($user->ID, 'auth_enabled', true);

		if ($authEnabled) {
			if (!isset($_SESSION['is_authing_id'])) {
				if (!$user) {
					header('Location:'. get_site_url());
					exit;
				}
			}

			$loggingTimestamp = $_SESSION['is_authing_ts'] ?? null;

			if ($loggingTimestamp && $loggingTimestamp > time()) {
				$this->denyAndRedirect();
				exit;
			}

			$_SESSION['is_authing_id'] = $user->ID;
			$_SESSION['is_authing_ts'] = time() + 60;
			wp_redirect('/multiauth/auth');
			exit;
		} else {
			return $redirect;
		}
	}

	public function forceAuth($template)
	{
		if (!get_query_var('auth')) {
			return $template;
		} else {

			$userId = get_current_user_id();
			// We make sure the user is logged out
			if ($userId !== 0) {
				wp_logout();
			}

			$userId = $_SESSION['is_authing_id'] ?? null;
			if (!$userId) {
				return $this->denyAndRedirect();
			}

			$authEnabled = get_user_meta($userId, 'auth_enabled', true);
			// $isLoggingIn = get_user_meta($user->ID, 'is_logging_in', true);
			$loggingTimestamp = $_SESSION['is_authing_ts'] ?? null;

			if ($loggingTimestamp && $loggingTimestamp <= time()) {
				return $this->denyAndRedirect();
			}

			$ajaxUrl = admin_url('admin-ajax.php');
			include __DIR__.'/views/auth.php';
			die;
		}
	}

	public function denyAndRedirect($url = null)
	{
		unset($_SESSION['is_authing_ts']);
		unset($_SESSION['is_authing_id']);
		if (get_current_user_id() !== 0) {
			wp_logout();
		}

		if (!$url) {
			$url = get_site_url();
		}
    	wp_redirect($url);
    	exit;
	}

	public function validate()
	{
		// $user = wp_get_current_user();
		$userId = $_SESSION['is_authing_id'] ?? null;

		if (!$userId) {
			unset($_SESSION['is_authing_id']);
			unset($_SESSION['is_authing_ts']);
			echo json_encode(['success'=>false, 'redirect'=>get_site_url()]);
			exit;
		}
	
		$time = $_SESSION['is_authing_ts'] ?? null;

		if (!$time || $time < time()) {
			unset($_SESSION['is_authing_id']);
			unset($_SESSION['is_authing_ts']);
			echo json_encode(['success'=>false, 'redirect'=>get_site_url()]);
			exit;
		}
		$secret = get_user_meta($userId, 'auth_secret', true);

		$code = $_POST['code'];

		include __DIR__.'/src/OTP.php';
		$otp = new OTP();
		$verified = $otp->verifyCode($secret, $code);

		if ($verified) {
			$user = get_user_by('id', $userId);
			unset($_SESSION['is_authing_id']);
			unset($_SESSION['is_authing_ts']);
			// update_user_meta($user->ID, 'is_logging_in', false);
			// wp_set_auth_cookie($user);
			wp_set_current_user($user->ID);
			wp_set_auth_cookie($user->ID);
			echo json_encode(['success'=>true, 'redirect'=>get_site_url()]);
			exit;
		} else {
			echo json_encode(['success'=>false]);
			exit;
			// $this->denyAndRedirect(get_site_url());
			// echo json_encode(['success'=>false]);
		}

		return json_encode(['success'=>false]);
		die;
	}

	public function showUserProfile($user)
	{
		if (wp_get_current_user()->ID === $user->ID) {
			$ajaxUrl = admin_url('admin-ajax.php');
			include __DIR__.'/views/fields.php';	
		}
	}

	public function editUserProfile($user)
	{
		if (wp_get_current_user()->ID === $user->ID) {
			$ajaxUrl = admin_url('admin-ajax.php');
			include __DIR__.'/views/fields.php';	
		}
	}

	/**
	 * Creates the routing rewrite rule for the courses URL
	 * @return void
	 */ 
	public function customRoutes()
	{
		add_rewrite_rule('^multiauth/auth/?$', 'index.php?auth=auth');
	    flush_rewrite_rules();
	}

	public function getRoute(string $routeName)
	{
		switch ($routeName) {
			case "enable": 
				return "enable";
				break;
		}

		return "#";
	}
}

$multiauth = new MultiAuth();
add_action('wp_ajax_enable_multiauth', [$multiauth, 'enableAuth']);
add_action('wp_ajax_disable_multiauth', [$multiauth, 'disableAuth']);
add_action('wp_ajax_nopriv_validate_multiauth', [$multiauth, 'validate']);
add_action('wp_ajax_validate_multiauth', [$multiauth, 'validate']);