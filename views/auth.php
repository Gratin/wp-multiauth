<?php

?>

<form>
	<input type="number" name="code" id="code" placeholder="code">
	<button id="validate" value="Valider">Valider</button>
</form>

<script type="text/javascript">
	document.addEventListener('DOMContentLoaded', (event) => {
		document.querySelector("#validate").addEventListener('click', (event) => {
			event.preventDefault();
			const formData = new FormData();
			formData.append('action', 'validate_multiauth');
			formData.append('code', document.querySelector("#code").val);

			fetch('<?php echo $ajaxUrl; ?>', {
				method: 'POST',
				credentials: 'same-origin',
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				body: 'action=validate_multiauth&code='+document.querySelector("#code").value
			}).then(response => response.json()).then((r) => {
				if (r.success === true) {
					if (r.redirect) {
						window.location.href = r.redirect;
					}
				} else {
					if (r.redirect !== void(0)) {
						window.location.href = r.redirect;
					}
					alert('Mauvais code');
				}
			});
		});
	});
</script>