<h3><?php echo _e('Authentication', 'multiauth'); ?></h3>
<table class="form-table">
 <tr>
     <th><label for="address"><?php _e("Enable", 'multiauth'); ?></label></th>
     <td>
     	<a href="#" id="enabled-auth"><?php _e("Enable", 'multiauth'); ?></a>
        <img src="#" id="qrcode">
     </td>
 </tr>
 </table>
 
 <script type="text/javascript">
 	document.addEventListener('DOMContentLoaded', (event) => {
 		document.querySelector("#enabled-auth").addEventListener('click', (event) => {
 			const formData = new FormData();
 			formData.append('action', 'enable_multiauth');

 			fetch('<?php echo $ajaxUrl; ?>', {
 				method: 'POST',
 				credentials: 'same-origin',
 				headers: {
 					'Content-Type': 'application/x-www-form-urlencoded'
 				},
 				body: 'action=enable_multiauth'
 			}).then(response => response.text()).then((r) => {
 				console.log(r);
 				document.querySelector("#qrcode").setAttribute('src', r);
 			});
 		});
 	});
 </script>